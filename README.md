# REST Assured tests

This repository contains a collection of automated tests based on REST Assured in Java. The tests are designed to
practice.

I used the application: https://automationexercise.com/
Creating all the test cases to try out solutions that I don't use every day at work and have fun :)

## Project assumptions

- [x] Use Allure reports
- [x] Use JUnit
- [x] Use Lombok
- [x] Use JavaFaker
- [x] User AssertJ
- [x] Use of the Builder design pattern

## Test cases

<details><summary>API 1: Get All Products List</summary>

    API URL: https://automationexercise.com/api/productsList
    Request Method: GET
    Response Code: 200
    Response JSON: All products list

</details>
<details><summary>API 2: POST To All Products List</summary>

    API URL: https://automationexercise.com/api/productsList
    Request Method: POST
    Response Code: 405
    Response Message: This request method is not supported.

</details>
<details><summary>API 3: Get All Brands List</summary>

    API URL: https://automationexercise.com/api/brandsList
    Request Method: GET
    Response Code: 200
    Response JSON: All brands list

</details>
<details><summary>API 4: PUT To All Brands List</summary>

    API URL: https://automationexercise.com/api/brandsList
    Request Method: PUT
    Response Code: 405
    Response Message: This request method is not supported.

</details>
<details><summary>API 5: POST To Search Product</summary>

    API URL: https://automationexercise.com/api/searchProduct
    Request Method: POST
    Request Parameter: search_product (For example: top, tshirt, jean)
    Response Code: 200
    Response JSON: Searched products list

</details>
<details><summary>API 6: POST To Search Product without search_product parameter</summary>

    API URL: https://automationexercise.com/api/searchProduct
    Request Method: POST
    Response Code: 400
    Response Message: Bad request, search_product parameter is missing in POST request.

</details>
<details><summary>API 7: POST To Verify Login with valid details</summary>

    API URL: https://automationexercise.com/api/verifyLogin
    Request Method: POST
    Request Parameters: email, password
    Response Code: 200
    Response Message: User exists!

</details>
<details><summary>API 8: POST To Verify Login without email parameter</summary>

    API URL: https://automationexercise.com/api/verifyLogin
    Request Method: POST
    Request Parameter: password
    Response Code: 400
    Response Message: Bad request, email or password parameter is missing in POST request.

</details>
<details><summary>API 9: DELETE To Verify Login</summary>

    API URL: https://automationexercise.com/api/verifyLogin
    Request Method: DELETE
    Response Code: 405
    Response Message: This request method is not supported.

</details>
<details><summary>API 10: POST To Verify Login with invalid details</summary>

    API URL: https://automationexercise.com/api/verifyLogin
    Request Method: POST
    Request Parameters: email, password (invalid values)
    Response Code: 404
    Response Message: User not found!

</details>
<details><summary>API 11: POST To Create/Register User Account</summary>

    API URL: https://automationexercise.com/api/createAccount
    Request Method: POST
    Request Parameters: name, email, password, title (for example: Mr, Mrs, Miss), birth_date, birth_month, birth_year, firstname, lastname, company, address1, address2, country, zipcode, state, city, mobile_number
    Response Code: 201
    Response Message: User created!

</details>
<details><summary>API 12: DELETE METHOD To Delete User Account</summary>

    API URL: https://automationexercise.com/api/deleteAccount
    Request Method: DELETE
    Request Parameters: email, password
    Response Code: 200
    Response Message: Account deleted!

</details>
<details><summary>API 13: PUT METHOD To Update User Account</summary>

    API URL: https://automationexercise.com/api/updateAccount
    Request Method: PUT
    Request Parameters: name, email, password, title (for example: Mr, Mrs, Miss), birth_date, birth_month, birth_year, firstname, lastname, company, address1, address2, country, zipcode, state, city, mobile_number
    Response Code: 200
    Response Message: User updated!

</details>
<details><summary>API 14: GET user account detail by email</summary>

    API URL: https://automationexercise.com/api/getUserDetailByEmail
    Request Method: GET
    Request Parameters: email
    Response Code: 200
    Response JSON: User Detail

</details>

## Running tests

Run test case for example only 'Get all brands':

`mvn clean test -DsuiteXmlFile="GetAllBrandsTest"`

Running all test cases:

`mvn clean test`

## Reports

The test report is saved in the 'target' folder. For run this report use below commands:

From root folder use:`cd target`

For run report:`allure serve`

For stop report use: **CTRL + C -> press 'Y' and click enter**

If you want to back to the root folder of the project use just: `cd ..`

If you want to clean your report use just `mvn clean`

## What's next?

Probably in the near future I will write API tests maybe in Postman, where values will be normally returned from the API, not inside the body.

## Contact

If you have questions, concerns, issues, or suggestions regarding this repository, please contact the author through
email: krystianselk@gmail.com