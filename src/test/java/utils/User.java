package utils;

import lombok.Getter;

@Getter
public class User {
    private String name, email, password, title, birthDate, birthMonth, birthYear, firstName, lastName, company,
            address1, address2, country, zipcode, state, city, mobileNumber;

    private User(UserBuilder builder) {
        this.name = builder.name;
        this.email = builder.email;
        this.password = builder.password;
        this.title = builder.title;
        this.birthDate = builder.birthDate;
        this.birthMonth = builder.birthMonth;
        this.birthYear = builder.birthYear;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.company = builder.company;
        this.address1 = builder.address1;
        this.address2 = builder.address2;
        this.country = builder.country;
        this.zipcode = builder.zipcode;
        this.state = builder.state;
        this.city = builder.city;
        this.mobileNumber = builder.mobileNumber;
    }

    public static class UserBuilder {
        private String name, email, password, title, birthDate, birthMonth, birthYear, firstName, lastName, company,
                address1, address2, country, zipcode, state, city, mobileNumber;

        public UserBuilder name(String name) { this.name = name; return this; }
        public UserBuilder email(String email) { this.email = email; return this; }
        public UserBuilder password(String password) { this.password = password; return this; }
        public UserBuilder title(String title) { this.title = title; return this; }
        public UserBuilder birthDate(String birthDate) { this.birthDate = birthDate; return this; }
        public UserBuilder birthMonth(String birthMonth) { this.birthMonth = birthMonth; return this; }
        public UserBuilder birthYear(String birthYear) { this.birthYear = birthYear; return this; }
        public UserBuilder firstName(String firstName) { this.firstName = firstName; return this; }
        public UserBuilder lastName(String lastName) { this.lastName = lastName; return this; }
        public UserBuilder company(String company) { this.company = company; return this; }
        public UserBuilder address1(String address1) { this.address1 = address1; return this; }
        public UserBuilder address2(String address2) { this.address2 = address2; return this; }
        public UserBuilder country(String country) { this.country = country; return this; }
        public UserBuilder zipcode(String zipcode) { this.zipcode = zipcode; return this; }
        public UserBuilder state(String state) { this.state = state; return this; }
        public UserBuilder city(String city) { this.city = city; return this; }
        public UserBuilder mobileNumber(String mobileNumber) { this.mobileNumber = mobileNumber; return this; }

        public User build() {
            return new User(this);
        }
    }
}
