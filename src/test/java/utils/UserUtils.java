package utils;

import api.ApiTestBase;
import com.github.javafaker.Faker;
import io.qameta.allure.Step;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import utils.response.ResponseUtils;

import static io.restassured.RestAssured.given;

public class UserUtils extends ApiTestBase {

    private static Faker faker = new Faker();

    public static User generateUserDetails() {
        return new User.UserBuilder()
                .name(faker.name().fullName())
                .email(faker.internet().emailAddress())
                .password(faker.internet().password())
                .title(faker.options().option("Mr", "Mrs", "Miss"))
                .birthDate(String.format("%02d", faker.number().numberBetween(1, 28)))
                .birthMonth(String.format("%02d", faker.number().numberBetween(1, 12)))
                .birthYear(Integer.toString(faker.number().numberBetween(1900, 2000)))
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .company(faker.company().name())
                .address1(faker.address().streetAddress())
                .address2(faker.address().secondaryAddress())
                .country(faker.address().country())
                .zipcode(faker.address().zipCode())
                .state(faker.address().state())
                .city(faker.address().city())
                .mobileNumber(faker.phoneNumber().cellPhone())
                .build();
    }

    @Step("Create user")
    public static User createUser() {
        User user = generateUserDetails();

        Response response = given()
                                .spec(defaultRequestSpec())
                                .formParam("name", user.getName())
                                .formParam("email", user.getEmail())
                                .formParam("password", user.getPassword())
                                .formParam("title", user.getTitle())
                                .formParam("birth_date", user.getBirthDate())
                                .formParam("birth_month", user.getBirthMonth())
                                .formParam("birth_year", user.getBirthYear())
                                .formParam("firstname", user.getFirstName())
                                .formParam("lastname", user.getLastName())
                                .formParam("company", user.getCompany())
                                .formParam("address1", user.getAddress1())
                                .formParam("address2", user.getAddress2())
                                .formParam("country", user.getCountry())
                                .formParam("zipcode", user.getZipcode())
                                .formParam("state", user.getState())
                                .formParam("city", user.getCity())
                                .formParam("mobile_number", user.getMobileNumber())
                            .when()
                                .post("/createAccount")
                            .then()
                                .extract().response();

        JsonPath jsonPath = ResponseUtils.getJsonPathFromResponse(response);

        if (response.getStatusCode() == 200 && jsonPath.getInt("responseCode") == 201 &&
                jsonPath.getString("message").equals("User created!")) {
            return user;
        } else {
            return null;
        }
    }

    @Step("Generate new user")
    public static User generateUpdatedUserDetails() {
        return new User.UserBuilder()
                .name(faker.name().fullName())
                .email(faker.internet().emailAddress())
                .password(faker.internet().password())
                .title(faker.options().option("Mr", "Mrs", "Miss"))
                .birthDate(String.format("%02d", faker.number().numberBetween(1, 28)))
                .birthMonth(String.format("%02d", faker.number().numberBetween(1, 12)))
                .birthYear(Integer.toString(faker.number().numberBetween(1900, 2000)))
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .company(faker.company().name())
                .address1(faker.address().streetAddress())
                .address2(faker.address().secondaryAddress())
                .country(faker.address().country())
                .zipcode(faker.address().zipCode())
                .state(faker.address().state())
                .city(faker.address().city())
                .mobileNumber(faker.phoneNumber().cellPhone())
                .build();
    }

    @Step("Get user id")
    public static String getUserId(String email) {
        Response response = given()
                                .spec(defaultRequestSpec())
                                .formParam("email", email)
                            .when()
                                .get("/getUserDetailByEmail")
                            .then()
                                .extract().response();

        JsonPath jsonPath = ResponseUtils.getJsonPathFromResponse(response);

        return jsonPath.getString("user.id");
    }

    @Step("Delete user")
    public static Response shouldDeleteUser(User createdUser) {
        return given()
                    .spec(defaultRequestSpec())
                    .formParam("email", createdUser.getEmail())
                    .formParam("password", createdUser.getPassword())
                .when()
                    .delete("/deleteAccount")
                .then()
                    .extract().response();
    }
}
