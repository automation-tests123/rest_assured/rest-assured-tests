package utils.assertions;

import io.qameta.allure.Step;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import utils.enums.HttpStatusCode;
import utils.enums.ResponseMessage;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ApiAssertions {

    @Step("Asserting that the status code is: {1}")
    public static void assertStatusCode(Response response, HttpStatusCode expectedStatus) {
        assertThat(response.statusCode())
                .as("HTTP status code should be: " + expectedStatus.getCode())
                .isEqualTo(expectedStatus.getCode());
    }

    @Step("Asserting that the body contain response code: {1}")
    public static void assertResponseCode(JsonPath jsonPath, HttpStatusCode expectedStatus) {
        assertThat(jsonPath.getInt("responseCode"))
                .as("Body should contain a response code: " + expectedStatus.getCode())
                .isEqualTo(expectedStatus.getCode());
    }

    @Step("Asserting that the error message is: {1}")
    public static void assertErrorMessage(JsonPath jsonPath, ResponseMessage errorMessage) {
        assertThat(jsonPath.getString("message"))
                .as("The error message should be: " + errorMessage.getMessage())
                .isEqualTo(errorMessage.getMessage());
    }

    @Step("Asserting that the content type is HTML with UTF-8 charset")
    public static void assertContentTypeIsHtmlUtf8(Response response) {
        assertThat(response.contentType())
                .as("Content type should be HTML with UTF-8 charset")
                .isEqualTo("text/html; charset=utf-8");
    }

    @Step("Asserting that the list is not empty")
    public static void assertListIsNotEmpty(List<?> items) {
        assertThat(items)
                .as("List should not be empty")
                .isNotEmpty();
    }

    @Step("Asserting that the object is not null")
    public static void assertObjectIsNotNull(Object object) {
        assertThat(object)
                .as("Object should not be null")
                .isNotNull();
    }
}
