package utils.enums;

import lombok.Getter;

@Getter
public enum ResponseMessage {
    METHOD_NOT_SUPPORTED("This request method is not supported."),
    MISSING_SEARCH_PRODUCT_PARAMETER("Bad request, search_product parameter is missing in POST request."),
    MISSING_EMAIL_OR_PASSWORD_PARAMETER("Bad request, email or password parameter is missing in POST request."),
    CREATED_USER("User created!"),
    USER_EXISTS("User exists!"),
    USER_NOT_FOUND("User not found!"),
    USER_UPDATED("User updated!"),
    ACCOUNT_DELETED("Account deleted!");

    private final String message;

    ResponseMessage(String message) {
        this.message = message;
    }
}
