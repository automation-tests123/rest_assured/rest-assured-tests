package utils.response;

import io.qameta.allure.Step;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ResponseUtils {

    @Step("Extracting response body as a string")
    public static String getResponseBodyAsString(Response response) {
        return response.getBody().asString();
    }

    @Step("Extracting JsonPath from response")
    public static JsonPath getJsonPathFromResponse(Response response) {
        String responseBody = getResponseBodyAsString(response);
        return new JsonPath(responseBody);
    }
}
