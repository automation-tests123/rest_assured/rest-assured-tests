package utils.logging;

import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import io.restassured.response.Response;

public class LoggingUtils {

    @Step("Logging response body")
    public static void logResponseBody(Response response) {
        Allure.addAttachment("Response Body", "application/json", response.getBody().asString());
    }
}
