package api;

import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeAll;

import static io.restassured.RestAssured.given;

public class ApiTestBase {

    @BeforeAll
    public static void setup() {
        setupBaseURI();
    }

    @Step("Setting up base URI for REST Assured")
    private static void setupBaseURI() {
        RestAssured.baseURI = "https://automationexercise.com/api";
    }

    @Step("Creating default request specification")
    protected static RequestSpecification defaultRequestSpec() {
        return given().log().all().header("Accept", "application/json");
    }
}
