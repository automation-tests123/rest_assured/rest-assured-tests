package tests;

import api.ApiTestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import utils.assertions.ApiAssertions;
import utils.enums.HttpStatusCode;
import utils.enums.ResponseMessage;
import utils.response.ResponseUtils;

import static io.restassured.RestAssured.given;

@Epic("Product Management")
@Feature("API Security and Method Restrictions")
public class PostToSearchProductWithoutParameterTest extends ApiTestBase {
    @Test
    @Story("Handling Missing Parameters in Product Search")
    @Description("Sending a POST request to /searchProduct without the required 'search_product' parameter " +
            "should return a status code of 400 indicating a bad request. " +
            "The response body should explicitly indicate that the 'search_product' parameter is missing.")
    public void shouldReturnErrorForMissingSearchParameter() {
        Response response = given()
                                .spec(defaultRequestSpec())
                            .when()
                                .post("/searchProduct")
                            .then()
                                .extract().response();

        JsonPath jsonPath = ResponseUtils.getJsonPathFromResponse(response);

        ApiAssertions.assertStatusCode(response, HttpStatusCode.OK);
        ApiAssertions.assertResponseCode(jsonPath, HttpStatusCode.BAD_REQUEST);
        ApiAssertions.assertErrorMessage(jsonPath, ResponseMessage.MISSING_SEARCH_PRODUCT_PARAMETER);
    }
}
