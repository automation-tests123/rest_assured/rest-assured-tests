package tests;

import api.ApiTestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import utils.assertions.ApiAssertions;
import utils.enums.HttpStatusCode;
import utils.enums.ResponseMessage;
import utils.response.ResponseUtils;

import static io.restassured.RestAssured.given;

@Epic("Brand Management")
@Feature("API Security and Method Restrictions")
public class PutToAllBrandsTest extends ApiTestBase {
    @Test
    @Story("Put request to all products list")
    @Description("Sending a PUT request to /brandsList should return a status code of 200 indicating successful request handling, " +
            "but the response body contains 'responseCode' of 405 indicating that the request method is not supported.")
    public void shouldReturnMethodNotAllowedForPutToBrandsList() {
        Response response = given()
                                .spec(defaultRequestSpec())
                            .when()
                                .put("/brandsList")
                            .then()
                                .extract().response();

        JsonPath jsonPath = ResponseUtils.getJsonPathFromResponse(response);

        ApiAssertions.assertStatusCode(response, HttpStatusCode.OK);
        ApiAssertions.assertResponseCode(jsonPath, HttpStatusCode.METHOD_NOT_ALLOWED);
        ApiAssertions.assertErrorMessage(jsonPath, ResponseMessage.METHOD_NOT_SUPPORTED);
    }
}
