package tests;

import api.ApiTestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.User;
import utils.UserUtils;
import utils.assertions.ApiAssertions;
import utils.enums.HttpStatusCode;
import utils.enums.ResponseMessage;
import utils.response.ResponseUtils;

import static io.restassured.RestAssured.given;

@Epic("User Management")
@Feature("Update")
public class PutToUpdateUserAccountTest extends ApiTestBase {
    private User createdUser;

    @BeforeEach
    public void setUp() {
        createdUser = UserUtils.createUser();
        ApiAssertions.assertObjectIsNotNull(createdUser);
    }

    @AfterEach
    public void deleteUser() {
        if (createdUser != null) {
            UserUtils.shouldDeleteUser(createdUser);
        }
    }

    @Test
    @Story("User Account Update")
    @Description("Verifies system's response to a PUT request at /updateAccount with all updated user details. " +
            "Ensures proper updating of user information by expecting a SUCCESS status and a specific response" +
            " message indicating user update.")
    public void shouldUpdateUser() {
        User updatedUser = UserUtils.generateUpdatedUserDetails();

        String userId = UserUtils.getUserId(createdUser.getEmail());

        Response response = given()
                                .spec(defaultRequestSpec())
                                .formParam("id", userId)
                                .formParam("email", createdUser.getEmail())
                                .formParam("password", createdUser.getPassword())
                                .formParam("name", updatedUser.getName())
                                .formParam("title", updatedUser.getTitle())
                                .formParam("birth_day", updatedUser.getBirthDate())
                                .formParam("birth_month", updatedUser.getBirthMonth())
                                .formParam("birth_year", updatedUser.getBirthYear())
                                .formParam("first_name", updatedUser.getFirstName())
                                .formParam("last_name", updatedUser.getLastName())
                                .formParam("company", updatedUser.getCompany())
                                .formParam("address1", updatedUser.getAddress1())
                                .formParam("address2", updatedUser.getAddress2())
                                .formParam("country", updatedUser.getCountry())
                                .formParam("state", updatedUser.getState())
                                .formParam("city", updatedUser.getCity())
                                .formParam("zipcode", updatedUser.getZipcode())
                            .when()
                                .put("/updateAccount")
                            .then()
                                .extract().response();


        JsonPath jsonPath = ResponseUtils.getJsonPathFromResponse(response);

        ApiAssertions.assertStatusCode(response, HttpStatusCode.OK);
        ApiAssertions.assertResponseCode(jsonPath, HttpStatusCode.OK);
        ApiAssertions.assertErrorMessage(jsonPath, ResponseMessage.USER_UPDATED);
    }
}
