package tests;

import api.ApiTestBase;
import io.qameta.allure.*;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import utils.assertions.ApiAssertions;
import utils.enums.HttpStatusCode;
import utils.logging.LoggingUtils;
import utils.response.ResponseUtils;

import java.util.List;

import static io.restassured.RestAssured.given;

@Epic("Brand Management")
@Feature("Listing")
public class GetAllBrandsTest extends ApiTestBase {

    @Test
    @Story("List all brands")
    @Description("Test to fetch all brands from the /brandsList endpoint and verify if the response is in HTML format and contains products.")
    public void shouldRetrieveAllBrandsInHtmlFormat() {
        retrieveAllBrandsList();
    }

    @Step("Retrieve all brands list and validate the response")
    private void retrieveAllBrandsList() {
        Response response = given()
                                .spec(defaultRequestSpec())
                            .when()
                                .get("/brandsList")
                            .then()
                                .extract().response();

        JsonPath jsonPath = ResponseUtils.getJsonPathFromResponse(response);

        List<String> brands = jsonPath.getList("brands");

        ApiAssertions.assertStatusCode(response, HttpStatusCode.OK);
        ApiAssertions.assertResponseCode(jsonPath, HttpStatusCode.OK);
        ApiAssertions.assertContentTypeIsHtmlUtf8(response);
        ApiAssertions.assertListIsNotEmpty(brands);

        LoggingUtils.logResponseBody(response);
    }
}
