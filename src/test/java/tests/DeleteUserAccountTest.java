package tests;

import api.ApiTestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.User;
import utils.UserUtils;
import utils.assertions.ApiAssertions;
import utils.enums.HttpStatusCode;
import utils.enums.ResponseMessage;
import utils.response.ResponseUtils;

@Epic("User Management")
@Feature("Delete")
public class DeleteUserAccountTest extends ApiTestBase {
    private User createdUser;

    @BeforeEach
    public void setUp() {
        createdUser = UserUtils.createUser();
        ApiAssertions.assertObjectIsNotNull(createdUser);
    }

    @Test
    @Story("Account Deletion")
    @Description("Verifies the system's response to a DELETE request at /deleteAccount with valid credentials. " +
            "Ensures proper deletion of user account by expecting an OK status and a confirmation message indicating account deletion.")
    public void shouldDeleteUser() {
        Response deletionResponse = UserUtils.shouldDeleteUser(createdUser);

        JsonPath jsonPath = ResponseUtils.getJsonPathFromResponse(deletionResponse);

        ApiAssertions.assertStatusCode(deletionResponse, HttpStatusCode.OK);
        ApiAssertions.assertResponseCode(jsonPath, HttpStatusCode.OK);
        ApiAssertions.assertErrorMessage(jsonPath, ResponseMessage.ACCOUNT_DELETED);
    }
}
