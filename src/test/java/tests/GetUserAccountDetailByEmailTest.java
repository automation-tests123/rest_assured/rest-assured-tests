package tests;

import api.ApiTestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.User;
import utils.UserUtils;
import utils.assertions.ApiAssertions;
import utils.enums.HttpStatusCode;
import utils.logging.LoggingUtils;
import utils.response.ResponseUtils;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@Epic("User Management")
@Feature("Listing")
public class GetUserAccountDetailByEmailTest extends ApiTestBase {
    private User createdUser;

    @BeforeEach
    public void setUp() {
        createdUser = UserUtils.createUser();
        ApiAssertions.assertObjectIsNotNull(createdUser);
    }

    @AfterEach
    public void deleteUser() {
        if (createdUser != null) {
            UserUtils.shouldDeleteUser(createdUser);
        }
    }

    @Test
    @Story("User Account Retrieval")
    @Description("Verifies system's response to a GET request at /getUserDetailByEmail with the user's email. " +
            "Ensures the retrieval of user's detailed information by comparing the response data with the " +
            "expected user details, expecting a SUCCESS status and accurate user information.")
    public void shouldGetUserDetails() {
        Response response = given()
                                .spec(defaultRequestSpec())
                                .queryParam("email", createdUser.getEmail())
                            .when()
                                .get("/getUserDetailByEmail")
                            .then()
                                .extract().response();

        LoggingUtils.logResponseBody(response);

        JsonPath jsonPath = ResponseUtils.getJsonPathFromResponse(response);

        ApiAssertions.assertStatusCode(response, HttpStatusCode.OK);
        ApiAssertions.assertResponseCode(jsonPath, HttpStatusCode.OK);

        assertThat(jsonPath.getString("user.name")).isEqualTo(createdUser.getName());
        assertThat(jsonPath.getString("user.title")).isEqualTo(createdUser.getTitle());
        assertThat(jsonPath.getString("user.birth_day")).isEqualTo(createdUser.getBirthDate());
        assertThat(jsonPath.getString("user.birth_month")).isEqualTo(createdUser.getBirthMonth());
        assertThat(jsonPath.getString("user.birth_year")).isEqualTo(createdUser.getBirthYear());
        assertThat(jsonPath.getString("user.first_name")).isEqualTo(createdUser.getFirstName());
        assertThat(jsonPath.getString("user.last_name")).isEqualTo(createdUser.getLastName());
        assertThat(jsonPath.getString("user.company")).isEqualTo(createdUser.getCompany());
        assertThat(jsonPath.getString("user.name")).isEqualTo(createdUser.getName());
        assertThat(jsonPath.getString("user.address1")).isEqualTo(createdUser.getAddress1());
        assertThat(jsonPath.getString("user.address2")).isEqualTo(createdUser.getAddress2());
        assertThat(jsonPath.getString("user.country")).isEqualTo(createdUser.getCountry());
        assertThat(jsonPath.getString("user.state")).isEqualTo(createdUser.getState());
        assertThat(jsonPath.getString("user.city")).isEqualTo(createdUser.getCity());
        assertThat(jsonPath.getString("user.zipcode")).isEqualTo(createdUser.getZipcode());
    }
}
