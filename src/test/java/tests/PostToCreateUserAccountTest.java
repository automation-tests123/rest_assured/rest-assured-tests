package tests;

import api.ApiTestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.User;
import utils.UserUtils;
import utils.assertions.ApiAssertions;

@Epic("User Management")
@Feature("Creating")
public class PostToCreateUserAccountTest extends ApiTestBase {
    private User createdUser;

    @BeforeEach
    public void setUp() {
        createdUser = UserUtils.createUser();
    }

    @AfterEach
    public void deleteUser() {
        if (createdUser != null) {
            UserUtils.shouldDeleteUser(createdUser);
        }
    }

    @Test
    @Story("User Creation")
    @Description("Sending a POST request to create a user")
    public void shouldCreateUser() {
        ApiAssertions.assertObjectIsNotNull(createdUser);
    }
}
