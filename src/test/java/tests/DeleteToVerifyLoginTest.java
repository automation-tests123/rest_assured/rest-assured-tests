package tests;

import api.ApiTestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import utils.assertions.ApiAssertions;
import utils.enums.HttpStatusCode;
import utils.enums.ResponseMessage;
import utils.response.ResponseUtils;

import static io.restassured.RestAssured.given;

@Epic("User Management")
@Feature("Verification")
public class DeleteToVerifyLoginTest extends ApiTestBase {
    @Test
    @Story("Validate improper request methods")
    @Description("Ensure sending a DELETE request to the login verification endpoint " +
            "returns an appropriate error message and status code, indicating " +
            "that the method is not allowed for this particular action.")
    public void shouldNotDeleteUser() {
        Response response = given()
                                .spec(defaultRequestSpec())
                            .when()
                                .delete("/verifyLogin")
                            .then()
                                .extract().response();

        JsonPath jsonPath = ResponseUtils.getJsonPathFromResponse(response);

        ApiAssertions.assertStatusCode(response, HttpStatusCode.OK);
        ApiAssertions.assertResponseCode(jsonPath, HttpStatusCode.METHOD_NOT_ALLOWED);
        ApiAssertions.assertErrorMessage(jsonPath, ResponseMessage.METHOD_NOT_SUPPORTED);
    }
}
