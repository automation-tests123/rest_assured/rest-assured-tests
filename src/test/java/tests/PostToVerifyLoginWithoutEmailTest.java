package tests;

import api.ApiTestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.User;
import utils.UserUtils;
import utils.assertions.ApiAssertions;
import utils.enums.HttpStatusCode;
import utils.enums.ResponseMessage;
import utils.response.ResponseUtils;

import java.util.Random;

import static io.restassured.RestAssured.given;

@Epic("User Management")
@Feature("Verification")
public class PostToVerifyLoginWithoutEmailTest extends ApiTestBase {
    private User createdUser;

    @BeforeEach
    public void setUp() {
        createdUser = UserUtils.createUser();
        ApiAssertions.assertObjectIsNotNull(createdUser);
    }

    @AfterEach
    public void deleteUser() {
        if (createdUser != null) {
            UserUtils.shouldDeleteUser(createdUser);
        }
    }

    @Test
    @Story("User Login with Incomplete Credentials")
    @Description("Verifies system's response to a POST request at /verifyLogin with a missing email or password." +
            " Ensures proper handling of incomplete credentials by expecting a BAD REQUEST status and specific error message.")
    public void shouldNotLoginUser() {
        Random rand = new Random();
        boolean missingEmail = rand.nextBoolean();

        RequestSpecification request = given().spec(defaultRequestSpec());

        if (!missingEmail) {
            request = request.formParam("email", createdUser.getEmail());
        } else {
            request = request.formParam("password", createdUser.getPassword());
        }

        Response response = request
                            .when()
                                .post("/verifyLogin")
                            .then()
                            .   extract().response();

        JsonPath jsonPath = ResponseUtils.getJsonPathFromResponse(response);

        ApiAssertions.assertStatusCode(response, HttpStatusCode.OK);
        ApiAssertions.assertResponseCode(jsonPath, HttpStatusCode.BAD_REQUEST);
        ApiAssertions.assertErrorMessage(jsonPath, ResponseMessage.MISSING_EMAIL_OR_PASSWORD_PARAMETER);
    }
}
