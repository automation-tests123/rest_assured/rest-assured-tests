package tests;

import api.ApiTestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import utils.assertions.ApiAssertions;
import utils.enums.HttpStatusCode;
import utils.logging.LoggingUtils;
import utils.response.ResponseUtils;

import java.util.List;

import static io.restassured.RestAssured.given;

@Epic("Product Management")
@Feature("Searching")
public class PostToSearchProductTest extends ApiTestBase {
    @Test
    @Story("Search for a Product Using POST")
    @Description("Sending a POST request to /searchProduct with a search query should return a status code of 200 indicating successful request handling " +
            "and the response body should contain a list of products related to the search query.")
    public void shouldSearchProduct() {
        Response response = given()
                                .spec(defaultRequestSpec())
                                .formParam("search_product", "tshirt")
                            .when()
                                .post("/searchProduct")
                            .then()
                                .extract().response();

        JsonPath jsonPath = ResponseUtils.getJsonPathFromResponse(response);

        List<String> products = jsonPath.getList("products");

        ApiAssertions.assertStatusCode(response, HttpStatusCode.OK);
        ApiAssertions.assertResponseCode(jsonPath, HttpStatusCode.OK);
        ApiAssertions.assertListIsNotEmpty(products);

        LoggingUtils.logResponseBody(response);
    }
}
