package tests;

import api.ApiTestBase;
import io.qameta.allure.*;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import utils.assertions.ApiAssertions;
import utils.enums.HttpStatusCode;
import utils.logging.LoggingUtils;
import utils.response.ResponseUtils;

import java.util.List;

import static io.restassured.RestAssured.given;

@Epic("Product Management")
@Feature("Listing")
public class GetAllProductsTest extends ApiTestBase {

    @Test
    @Story("List all products")
    @Description("Test to fetch all products from the /productsList endpoint and verify if the response is in HTML format and contains products.")
    public void shouldRetrieveAllProductsInHtmlFormat() {
        retrieveAllProductsList();
    }

    @Step("Retrieve all products list and validate the response")
    private void retrieveAllProductsList() {
        Response response = given()
                                .spec(defaultRequestSpec())
                            .when()
                                .get("/productsList")
                            .then()
                                .extract().response();

        JsonPath jsonPath = ResponseUtils.getJsonPathFromResponse(response);

        List<String> products = jsonPath.getList("products");

        ApiAssertions.assertStatusCode(response, HttpStatusCode.OK);
        ApiAssertions.assertResponseCode(jsonPath, HttpStatusCode.OK);
        ApiAssertions.assertContentTypeIsHtmlUtf8(response);
        ApiAssertions.assertListIsNotEmpty(products);

        LoggingUtils.logResponseBody(response);
    }
}
