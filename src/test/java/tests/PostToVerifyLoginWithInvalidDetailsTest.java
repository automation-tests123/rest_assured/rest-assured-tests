package tests;

import api.ApiTestBase;
import com.github.javafaker.Faker;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import utils.assertions.ApiAssertions;
import utils.enums.HttpStatusCode;
import utils.enums.ResponseMessage;
import utils.response.ResponseUtils;

import static io.restassured.RestAssured.given;

@Epic("User Management")
@Feature("Verification")
public class PostToVerifyLoginWithInvalidDetailsTest extends ApiTestBase {

    @Test
    @Story("Verifying Non-Existing User Login")
    @Description("Tests login with random credentials to ensure correct handling of non-existing users, " +
            "expecting NOT FOUND status and error message.")
    public void shouldNotLoginUser() {
        Faker faker = new Faker();

        Response response = given()
                                .spec(defaultRequestSpec())
                                .formParam("email", faker.internet().emailAddress())
                                .formParam("password", faker.internet().password())
                            .when()
                                .post("/verifyLogin")
                            .then()
                                .extract().response();

        JsonPath jsonPath = ResponseUtils.getJsonPathFromResponse(response);

        ApiAssertions.assertStatusCode(response, HttpStatusCode.OK);
        ApiAssertions.assertResponseCode(jsonPath, HttpStatusCode.NOT_FOUND);
        ApiAssertions.assertErrorMessage(jsonPath, ResponseMessage.USER_NOT_FOUND);
    }
}
