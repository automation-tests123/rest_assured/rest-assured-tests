package tests;

import api.ApiTestBase;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import utils.User;
import utils.UserUtils;
import utils.assertions.ApiAssertions;
import utils.enums.HttpStatusCode;
import utils.enums.ResponseMessage;
import utils.response.ResponseUtils;

import static io.restassured.RestAssured.given;

@Epic("User Management")
@Feature("Verification")
public class PostToVerifyLoginTest extends ApiTestBase {
    private User createdUser;

    @BeforeEach
    public void setUp() {
        createdUser = UserUtils.createUser();
        ApiAssertions.assertObjectIsNotNull(createdUser);
    }

    @AfterEach
    public void deleteUser() {
        if (createdUser != null) {
            UserUtils.shouldDeleteUser(createdUser);
        }
    }

    @Test
    @Story("User login verification")
    @Description("Sending a POST request to verify login")
    public void shouldLoginUser() {
        Response response = given()
                                .spec(defaultRequestSpec())
                                .formParam("email", createdUser.getEmail())
                                .formParam("password", createdUser.getPassword())
                            .when()
                                .post("/verifyLogin")
                            .then()
                                .extract().response();

        JsonPath jsonPath = ResponseUtils.getJsonPathFromResponse(response);

        ApiAssertions.assertStatusCode(response, HttpStatusCode.OK);
        ApiAssertions.assertResponseCode(jsonPath, HttpStatusCode.OK);
        ApiAssertions.assertErrorMessage(jsonPath, ResponseMessage.USER_EXISTS);
    }
}
